#!/bin/bash
function go_install(){
go get -v github.com/projectdiscovery/notify/cmd/notify
go get -v github.com/cgboal/sonarsearch/crobat
go get -v github.com/gwen001/github-subdomains
go get -v github.com/KathanP19/Gxss
# check if install improved go get -v github.com/dwisiswant0/crlfuzz/cmd/crlfuzz
go get -v github.com/dwisiswant0/cf-check
go get  -v github.com/lc/subjs
go get -v github.com/tomnomnom/anew
go get -v github.com/tomnomnom/qsreplace
go get -v github.com/gwen001/github-endpoints
go get -v github.com/tomnomnom/waybackurls
go get -v github.com/tomnomnom/gron
}

go_install
