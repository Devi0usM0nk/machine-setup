FROM archlinux:base
RUN mkdir /root/internals
RUN mkdir -p /root/Documents/Tools
COPY setup.sh /root/internals/setup.sh
COPY pip_install.sh /root/internals/pip_install.sh
COPY go_install.sh /root/internals/go_install.sh
COPY github_install.sh /root/internals/github_install.sh
COPY install_list /root/internals/install_list
COPY update.sh /root/internals/update.sh
COPY binary_install.sh /root/internals/binary_install.sh
COPY misc_install.sh /root/internals/misc_install.sh
COPY postinstall.sh /root/internals/postinstall.sh

RUN chmod +x /root/internals/setup.sh
RUN chmod +x /root/internals/github_install.sh
RUN chmod +x /root/internals/pip_install.sh
RUN chmod +x /root/internals/go_install.sh
RUN chmod +x /root/internals/update.sh
RUN chmod +x /root/internals/binary_install.sh
RUN chmod +x /root/internals/misc_install.sh
RUN chmod +x /root/internals/postinstall.sh

RUN /root/internals/setup.sh &
RUN /root/internals/github_install.sh &
RUN /root/internals/go_install.sh &
RUN /root/internals/pip_install.sh &
RUN /root/internals/binary_install.sh &
RUN /root/internals/misc_install.sh &
RUN /root/internals/postinstall.sh &


