#!/bin/bash
tools_dir=$HOME/Documents/Tools

function main(){
	byp4xx_install
	githunter_install
	feroxbuster_install
	dnsvalidator_install
	get_all_txt
	wp-allplugin
	wp-vuln | sort -u | sed '/^$/d' > $tools_dir/Wordlists/wp-vulnlist
	# Labs
	pentestlab
	cloudgoat
	slurp
}

function taskopen(){
	git clone https://github.com/jschlatow/taskopen.git $HOME/customizations/apps/taskopen
	cd $HOME/customizations/apps/taskopen 
	make PREFIX=usr
	sudo make PREFIX=usr install
}
# slurp by https://github.com/0xbharath/slurp.git
function slurp(){
	git clone https://github.com/0xbharath/slurp.git $tools_dir/Cloud/slurp
	cd $tools_dir/Cloud/slurp
	go build
	mv ./slurp $HOME/.local/bin/slurp
	mv ./permutations.json $HOME/.local/share
}
# CloudGoat
function cloudgoat(){
	git clone https://github.com/RhinoSecurityLabs/cloudgoat.git $tools_dir/Labs/cloudgoat
	cd $tools_dir/Labs/cloudgoat
	pip3 install -r ./core/python/requirements.txt
	chmod u+x cloudgoat.py
	./cloudgoat.py config profile
	./cloudgoat.py config whitelist --auto
}

# PentestLab
function pentestlab(){
git clone https://github.com/eystsen/pentestlab.git $tools_dir/Labs/pentestlab
}
# Get powercat
function powercat(){
curl "https://raw.githubusercontent.com/besimorhino/powercat/master/powercat.ps1" > ~/Documents/Tools/Shell
}

# Get grex install
function grex(){
	git clone https://github.com/pemistahl/grex.git $tools_dir/Misc/grex
	cargo install grex
	cp $HOME/.cargo/bin/grex $HOME/.local/bin/
}
# Get list of all vulnerable  wordpress plugins
function wp-vuln(){
my_array=$(searchsploit wordpress | grep -i "plugin" | grep -v "Joomla"| grep -v "Multiple" | awk -F 'Plugin ' '{print $2}' | awk -F' - ' '{print $1}' | sed 's/[0-9\.]//g' | awk -F '<' '{print $1}'|sort -u | tr ' ' ';')

result=$(cat $HOME/Documents/Tools/Wordlists/wp-allplugins)

function vuln-search()
{
    internal=$(echo $1|sed  's/\;/\n/g')
    for x in ${internal[@]}
    do
    if [ -n "$x" ]; then
	result=$(echo $result | tr ' ' '\n' | grep -i "$x" 2>/dev/null)
    fi
    done
    echo $result | sed 's/ /\n/g'
}
for y in ${my_array[@]}
do
    vuln-search $y &
done
wait
}

# Get List all wordpress plugins
function wp-allplugin(){ # Get All woordpress plugins
    curl -s "http://plugins.svn.wordpress.org/" | grep href | awk -F '/">'  '{print $2}' | awk -F '/</a>' '{print $1}' | grep -v '<' | sed '/^$/d' > $tools_dir/Wordlists/wp-allplugins 
}

# byp4xx by lobuhi
function byp4xx_install(){
git clone https://github.com/lobuhi/byp4xx.git $tools_dir/Web/byp4xx
chmod u+x $tools_dir/Web/byp4xx/byp4xx.sh
}

# Jason haddix githunter
function githunter_install(){
echo "#!/bin/bash" > $tools_dir/Web/githunter.sh
curl 'https://gist.githubusercontent.com/jhaddix/77253cea49bf4bd4bfd5d384a37ce7a4/raw/ca20c5d8543ef3404b6032938162790636f89241/Github%2520bash%2520generated%2520search%2520links%2520(from%2520hunter.sh)' >> $tools_dir/Web/githunter.sh
chmod u+x $tools_dir/Web/githunter.sh
cp $tools_dir/Web/githunter.sh ~/.local/bin/
}

# feroxbuster
function feroxbuster_install(){
git clone https://github.com/epi052/feroxbuster.git
mv feroxbuster $tools_dir/Web/feroxbuster
cd $tools_dir/Web/feroxbuster/
./install-nix.sh
mv feroxbuster $HOME/.local/bin/
}

# dnsvalidator
function dnsvalidator_install(){
git clone https://github.com/vortexau/dnsvalidator.git
mv dnsvalidator $tools_dir/Web/dnsvalidator 
cd $tools_dir/Web/dnsvalidator
sudo python3 setup.py install
}

# all.txt
function get_all_txt(){
wget https://gist.githubusercontent.com/jhaddix/f64c97d0863a78454e44c2f7119c2a6a/raw/96f4e51d96b2203f19f6381c8c545b278eaa0837/all.txt 
mv all.txt $tools_dir/Wordlists/
}

function get_rockyou(){
wget https://gitlab.com/kalilinux/packages/wordlists/-/raw/kali/master/rockyou.txt.gz
gzip -d rockyou.txt.gz
mv rockyou.txt $tools_dir/Wordlists
}

# Calling main function to call all functions
main
