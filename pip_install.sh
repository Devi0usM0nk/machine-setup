#!/bin/bash
function pip_install (){
# Upgrade pip
python -m pip install --upgrade pip

# codenamize (Cloud)
pip install codenamize --user

# boto3 
pip install boto3

#altdns
python2 -m pip install py-altdns

#ueberzug
pip install ueberzug

# python-language-server
pip install python-language-server
}

