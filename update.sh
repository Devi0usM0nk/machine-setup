#!/bin/bash
tools_dir=$HOME/Documents/Tools

function main(){
    binary_update
    github_update
    pip_update
    go_update
    wp-allplugin
    wp-vuln | sort -u | sed '/^$/d' > $tools_dir/Wordlists/wp-vulnlist
}

# Update all Misc
#function misc_update(){
#    # update aws_recon check after sept 2021
#    gem update ox
#    gem update aws_recon
#}
# Update all Binary Install
function binary_update(){
# CLI53 Sorce :- github
cd $HOME/.local/bin
wget https://github.com/barnybug/cli53/releases/download/0.8.18/cli53-linux-amd64
chmod +x cli53-linux-amd64
mv cli53-linux-amd64 cli53
}

# Get list of all vulnerable  wordpress plugins
function wp-vuln(){
my_array=$(searchsploit wordpress | grep -i "plugin" | grep -v "Joomla"| grep -v "Multiple" | awk -F 'Plugin ' '{print $2}' | awk -F' - ' '{print $1}' | sed 's/[0-9\.]//g' | awk -F '<' '{print $1}'|sort -u | tr ' ' ';')
}

# Get All woordpress plugins
function wp-allplugin(){ 
    curl -s "http://plugins.svn.wordpress.org/" | grep href | awk -F '/">'  '{print $2}' | awk -F '/</a>' '{print $1}' | grep -v '<' | sed '/^$/d' > $tools_dir/Wordlists/wp-allplugins 
}



function pip_update (){
# Upgrade pip
python -m pip install --upgrade pip

# codenamize (Cloud)
pip install codenamize --user --upgrade

# boto3 
pip install boto3 --upgrade
#altdns
python2 -m pip install --upgrade py-altdns

#ueberzug
pip install --upgrade ueberzug

# python-language-server
pip install python-language-server --upgrade
}

function go_update(){
go get -u -f -v -t -insecure github.com/projectdiscovery/notify/cmd/notify
go get -u -f -v -t -insecure github.com/cgboal/sonarsearch/crobat
go get -u -f -v -t -insecure github.com/gwen001/github-subdomains
go get -u -f -v -t github.com/KathanP19/Gxss
go get -u -f -v -t github.com/dwisiswant0/cf-check
go get -u -f -v -t github.com/lc/subjs
go get -u -f -v -t github.com/tomnomnom/anew
go get -u -f -v -t github.com/tomnomnom/qsreplace
go get -u -f -v -t github.com/gwen001/github-endpoints
go get -u -f -v -t github.com/tomnomnom/waybackurls
go get -u -f -v -t github.com/tomnomnom/gron
}

function github_update(){
#taskopen
git clone https://github.com/jschlatow/taskopen.git $HOME/customizations/apps/taskopen
cd $HOME/customizations/apps/taskopen 
make PREFIX=usr
sudo make PREFIX=usr install

# slurp
git clone https://github.com/0xbharath/slurp.git $tools_dir/Cloud/slurp
cd $tools_dir/Cloud/slurp
go build
mv ./slurp $HOME/.local/bin/slurp
mv ./permutations.json $HOME/.local/share

# powercat
curl "https://raw.githubusercontent.com/besimorhino/powercat/master/powercat.ps1" > $tools_dir/Shell

# Get grex install
git clone https://github.com/pemistahl/grex.git $tools_dir/Misc/grex
cargo install grex
cp $HOME/.cargo/bin/grex $HOME/.local/bin/

# byp4xx by lobuhi
cd $HOME/Documents/Tools/Web/byp4xx
git pull https://github.com/lobuhi/byp4xx.git 
chmod u+x $tools_dir/Web/byp4xx/byp4xx.sh

# Jason haddix githunter
echo "#!/bin/bash" > $tools_dir/Web/githunter.sh
curl 'https://gist.githubusercontent.com/jhaddix/77253cea49bf4bd4bfd5d384a37ce7a4/raw/ca20c5d8543ef3404b6032938162790636f89241/Github%2520bash%2520generated%2520search%2520links%2520(from%2520hunter.sh)' >> $tools_dir/Web/githunter.sh
chmod u+x $tools_dir/Web/githunter.sh
cp $tools_dir/Web/githunter.sh ~/.local/bin/

# feroxbuster
cd $tools_dir/Web/feroxbuster
git pull https://github.com/epi052/feroxbuster.git
./install-nix.sh
mv feroxbuster $HOME/.local/bin/

# dnsvalidator
cd $tools_dir/Web/dnsvalidator 
git pull https://github.com/vortexau/dnsvalidator.git
sudo python3 setup.py install

#jason haddix all.txt
wget https://gist.githubusercontent.com/jhaddix/f64c97d0863a78454e44c2f7119c2a6a/raw/96f4e51d96b2203f19f6381c8c545b278eaa0837/all.txt 
mv all.txt $tools_dir/Wordlists/
}
#Calling main funtion
main
